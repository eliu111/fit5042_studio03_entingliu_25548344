package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	private Set<Property> compList;
	
	public ComparePropertySessionBean() {
		compList = new HashSet<Property>();
	}
	
	// compList is a set, there is no need to check for duplicates.
	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		compList.add(property);
	}

	// get the price and number of rooms from compare list property,
	// if best price per room is lesser than the previous property,
	// update the bestID of propertyID.
	@Override
	public int getBestPerRoom() {
		// TODO Auto-generated method stub
		int bestID = 0;
		int numOfRooms = 0;
		double price;
		double bestPerRoom = 100000000.00;
		for (Property pty : compList) {
			price = pty.getPrice();
			numOfRooms = pty.getNumberOfBedrooms();
			if (price / numOfRooms < bestPerRoom) {
				bestID = pty.getPropertyId();
			}
		}
		return bestID;
	}
	
	// if property set compList contains the property given
	// remove the property from compList
	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		for (Property pty : compList) {
			if (pty.getPropertyId() == property.getPropertyId()) {
				compList.remove(pty);
				break;
			}
		}
	}

	@Override
	public CompareProperty create() throws CreateException, RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
