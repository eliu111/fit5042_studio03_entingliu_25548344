/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.entities.Loan;
import fit5042.tutex.calculator.MonthlyPaymentCalculator;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 * order for students to practice.
 */

// When ManagedBean has attribute eager = true, and depending on the scope declared,
// The instantiation and storing of the instance must happen "before" any request are serviced.
// Otherwise, the "lazy" initialization is used and this bean will only be instantiate when
// it is requested.
// Ref: https://docs.oracle.com/javaee/7/api/javax/faces/bean/ManagedBean.html#eager--
@ManagedBean(name = "loanManagedBean", eager = true)
@SessionScoped

public class LoanManagedBean implements Serializable {
	// EJBs is best done using dependency injection,
	// Since it's easy to implement
    @EJB
    MonthlyPaymentCalculator calculator;
	
	private Loan loan;

    public LoanManagedBean() {
        this.loan = new Loan();

    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public String calculate() {
        
        double monthlyPayment;
        //You will need to modify the monthlyPayment value and set it as the monthly payment attribute value into the loan instance
        //Please complete this method starts from here
        monthlyPayment = this.calculator.calculate(loan.getPrinciple(), loan.getNumberOfYears(), loan.getInterestRate());
        this.loan.setMonthlyPayment(monthlyPayment);
        
        // why return index here???
        return "index";
    }
}
